import sys
import os
from cfengine import PromiseModule, ValidationError
from subprocess import run, PIPE, STDOUT

class PromiseTypeModule(PromiseModule):
    def validate_promise(self, promiser, attributes):
        if not promiser.startswith("/"):
            raise ValidationError(f"File path '{promiser}' must be absolute")
        if not "from" in attributes:
            raise ValidationError("Lacks 'from' attribute in rsync promise type")
        if type(attributes["from"]) is not str:
            raise ValidationError(f"'from' must be string for rsync promise types")

    def evaluate_promise(self, promiser, attributes):
        self.validate_promise(promiser, attributes)

        dst = promiser
        src = attributes["from"]

        # TODO: parse other attributes

        cmd = ["rsync", "-ai", src, dst]

        try:
            result = run(cmd, check=True, stdout=PIPE, stderr=STDOUT, universal_newlines=True)
        except:
            # any error, also if process exited with a non-zero exit code
            self.promise_not_kept()
            return

        if result.stdout == '':
            # no output - so no change
            self.promise_kept()
        else:
            # rsync printed something - so it did something
            for line in result.stdout.splitlines():
                self.log_info(line)
            self.promise_repaired()

if __name__ == "__main__":
    PromiseTypeModule().start()
