sudo yum -y install python3

mkdir -p /home/vagrant/.cfagent/bin
touch /home/vagrant/.cfagent/bin/cf-promises
chmod a+x /home/vagrant/.cfagent/bin/cf-promises

mkdir -p /home/vagrant/.cfagent/inputs
cp /vagrant/custom-promises/promises.cf /home/vagrant/.cfagent/inputs
chmod 600 /home/vagrant/.cfagent/inputs/promises.cf

curl https://raw.githubusercontent.com/cfengine/core/master/docs/custom_promise_types/cfengine.py >/vagrant/custom-promises/cfengine.py
